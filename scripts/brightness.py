#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import subprocess
import sys
from glob import glob


def get_cards():
    """
    Get a list of all graphics cards that we can set brightness for.
    """
    cards = glob('/sys/class/backlight/*')
    return cards


def get_card_info(card_path):
    """
    Get the brightness info for the given card at `card_path`.

    :param str card_path: Path of the card info under /sys/class/backlight/
    :returns: dict of {max_brightness(int), current_brightness(int), percentage(int)}
    """
    with open(os.path.join(card_path, 'max_brightness'), 'r') as maxbright:
        max_brightness = int(maxbright.read())

    with open(os.path.join(card_path, 'brightness'), 'r') as bright:
        brightness = int(bright.read())

    perc = int(brightness * 100 / max_brightness)

    return {
        'max_brightness': max_brightness,
        'brightness': brightness,
        'percentage': perc
    }


def sync_ext_monitor():
    """
    Set the same brightness on the external monitor as the value on the laptop screen.
    """
    rc = subprocess.call('xrandr -q | grep HDMI1', shell=True)

    # if grep succeeds, return code is 0, which means the monitor is connected
    if rc == 0:
        # read laptop brightness value
        res = subprocess.check_output('xbacklight -get', shell=True).decode('utf8')
        br_value = float(res) / 100.0
        # set brightness on the HDMI screen
        subprocess.call(f'xrandr --output HDMI1 --brightness {br_value}', shell=True)


def main():
    up_or_down = sys.argv[1]  # 'up' or 'down'
    if len(sys.argv) >= 3:
        steps = sys.argv[2]  # steps to increase / decrease
    else:
        steps = "10"

    cmd = '''
        echo "shell_notify_bar('Brightness', {perc}, 20)" | awesome-client
    '''

    if up_or_down == 'up':
        flag = '-inc'
    elif up_or_down == 'down':
        flag = '-dec'
    else:
        raise ValueError(f'First argument must be "up" or "down", got {up_or_down}')

    # increase / decrease brightness on laptop screen
    subprocess.call(['xbacklight', flag, steps])

    # set the same brightness on external screen
    sync_ext_monitor()

    # get percentage and show it in an awesomewm popup
    info = get_card_info(get_cards()[0])
    subprocess.call(cmd.format(perc=info['percentage']), shell=True)


if __name__ == '__main__':
    main()
