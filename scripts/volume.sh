#!/bin/bash
#
# volume.sh
#
# $1 = up/down/mute
# $2 = steps to increase/decrease (%)

[[ "$1" = "up" ]]   && amixer -D pulse set Master "$2"%+
[[ "$1" = "down" ]] && amixer -D pulse set Master "$2"%-
[[ "$1" = "mute" ]] && amixer -D pulse set Master toggle

VOL=$(amixer get -D pulse Master | grep "Front Left": | sed 's|[^[]*\[\([0-9]*\).*|\1|')
if [[ $(amixer get -D pulse Master | grep "\[off\]") ]]; then
    TITLE="'Volume Muted'";
    BAR_COLOR="'#FF0000'";
    END_MARKER="'X'";
else
    TITLE="'Volume'";
    BAR_COLOR="'#00FF00'";
    END_MARKER="'>'";

    # Fix unbalanced left-right channel when multiple instances run. A bit janky.
    amixer -D pulse set Master $VOL%,$VOL%
fi

BAR_LENGTH=24
echo "shell_notify_bar($TITLE, $VOL, $BAR_LENGTH, $BAR_COLOR, $END_MARKER)" | awesome-client
